namespace Agrostream.ParsingLibrary
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    public static class Parser
    {
        public static IList<Customer> Parse(string filePath)
        {
            if (!File.Exists(filePath))
            {
                throw new ParserException($"File does not exist: {filePath}");
            }

            var lines = File.ReadAllLines(filePath);
            var results = new List<Customer>();
            
            for (var index = 0; index < lines.Length; index++)
            {
                var line = lines[index];

                var lineParts = line.Split(',');
                if (!int.TryParse(lineParts[0], out int id))
                {
                    throw new ParserException($"Cannot parse ID at line {index}.");
                }

                if (string.IsNullOrWhiteSpace(lineParts[1]))
                {
                    throw new ParserException($"Cannot parse first name at line {index}.");
                }

                if (string.IsNullOrWhiteSpace(lineParts[2]))
                {
                    throw new ParserException($"Cannot parse last name at line {index}.");
                }
                
                if (!int.TryParse(lineParts[3], out int customerStatus))
                {
                    throw new ParserException($"Cannot parse customer status at line {index}.");
                }

                if (!Enum.IsDefined(typeof(CustomerStatus), customerStatus))
                {
                    throw new ParserException($"Customer status at line {index} is not supported.");
                }

                results.Add(new Customer
                {
                    Id = id,
                    FirstName = lineParts[1],
                    LastName = lineParts[2],
                    Status = (CustomerStatus)customerStatus
                });
            }

            return results;
        }
    }
}